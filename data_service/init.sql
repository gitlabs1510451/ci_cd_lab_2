-- Создание таблицы преподавателей
CREATE TABLE teachers (
    teacher_id SERIAL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL
);

-- Создание таблицы студентов
CREATE TABLE students (
    student_id SERIAL PRIMARY KEY,
    last_name VARCHAR(50) NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    birthdate DATE NOT NULL
);

-- Создание таблицы предметов
CREATE TABLE subjects (
    subject_id SERIAL PRIMARY KEY,
    subject_name VARCHAR(100) NOT NULL
);

-- Создание таблицы оценок студентов
CREATE TABLE grades (
    grade_id SERIAL PRIMARY KEY,
    subject_id INT REFERENCES subjects(subject_id),
    student_id INT REFERENCES students(student_id),
    exam_date DATE NOT NULL,
    teacher_id INT REFERENCES teachers(teacher_id),
    score INT NOT NULL
);

-- Заполнение таблицы преподавателей
INSERT INTO teachers (full_name) VALUES
    ('Иванов Иван Иванович'),
    ('Петров Петр Петрович'),
    ('Сидоров Сидор Сидорович');

-- Заполнение таблицы студентов
INSERT INTO students (last_name, first_name, birthdate) VALUES
    ('Иванов', 'Алексей', '2002-01-01'),
    ('Петров', 'Дмитрий', '2002-02-02'),
    ('Сидоров', 'Иван', '2002-03-03'),
    ('Козлов', 'Андрей', '2001-04-04'),
    ('Смирнов', 'Александр', '2003-05-05');

-- Заполнение таблицы предметов
INSERT INTO subjects (subject_name) VALUES
    ('Математика'),
    ('Физика'),
    ('Химия');

-- Заполнение таблицы оценок студентов
INSERT INTO grades (subject_id, student_id, exam_date, teacher_id, score) VALUES
    (1, 1, '2021-01-10', 1, 4),
    (1, 2, '2021-01-10', 1, 3),
    (1, 3, '2021-01-10', 1, 5),
    (2, 2, '2021-01-15', 2, 4),
    (2, 4, '2021-01-15', 2, 5),
    (2, 5, '2021-01-15', 2, 4),
    (3, 1, '2021-01-20', 3, 4),
    (3, 3, '2021-01-20', 3, 5);
