import psycopg2

def connect_to_db():
    conn = psycopg2.connect(
        database="postgres",
        user="postgres",
        password="admin",
        host="data_service",
        port="5432"
    )
    return conn

def get_worst_performance():
    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute("""
	-- Запрос для нахождения ФИО студента с худшей успеваемостью
	WITH student_worst_performance AS (
	    SELECT s.student_id, s.last_name, s.first_name, AVG(g.score) AS avg_score
	    FROM students s
	    JOIN grades g ON s.student_id = g.student_id
	    GROUP BY s.student_id, s.last_name, s.first_name
	    ORDER BY avg_score ASC
	    LIMIT 1
	)
	SELECT last_name, first_name
	FROM student_worst_performance;
    """)
    result = cursor.fetchall()
    print('ФИО:', result[0][0], result[0][1])
    print()
    cursor.close()
    conn.close()
    
def get_grades():
    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute("""
	-- Запрос для вывода всех оценок этого студента
	WITH student_worst_performance AS (
	    SELECT s.student_id, s.last_name, s.first_name, AVG(g.score) AS avg_score
	    FROM students s
	    JOIN grades g ON s.student_id = g.student_id
	    GROUP BY s.student_id, s.last_name, s.first_name
	    ORDER BY avg_score ASC
	    LIMIT 1
	)
	SELECT subject_name, score
	FROM student_worst_performance
	JOIN grades g ON student_worst_performance.student_id = g.student_id
	JOIN subjects s ON g.subject_id = s.subject_id;
    """)
    result = cursor.fetchall()
    
    # Печать заголовков столбцов
    print("| Предмет   | Оценка |")
    print("---------------------")
    
    # Печать данных в виде таблицы
    for subject, score in result:
        print("| {:<10} | {:<6} |".format(subject, score))

    
    cursor.close()
    conn.close()

def main():
    get_worst_performance()
    get_grades()

if __name__ == "__main__":
    main()
